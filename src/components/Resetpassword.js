import React, { Component } from 'react'
import axios from 'axios';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { useHistory } from 'react-router-dom';




export default class resetPassword extends Component {


    constructor(props) {
        super(props);
        this.state = { 
            linkDate:"",
            medium:"", 
            emailOrPhone:"",
            newPassword: "",
            confirmationCode: "", 
            errors: {},
            isEmail: false
        };
    }

    componentDidMount(){
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);

        const userId = urlParams.get('userid');
        const confirmationCode = urlParams.get('code');
        const emailOrPhone = urlParams.get('email');
        const medium = "email";
        const isEmail = true;
        // console.log(userId, confirmationCode, emailOrPhone);

        if (userId && confirmationCode && emailOrPhone){
            this.setState({userId, confirmationCode, emailOrPhone, medium, isEmail});
        } 
    }
   
    handleInput = e => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
    }
    handleForm = e => {
        e.preventDefault();
        if (this.state.emailOrPhone === '') {
            NotificationManager.warning("email or Phone is Required");
            return false;
        }
        // const data = { email: this.state.email, };
        // console.log(data)
        axios
            .post("https://wu89z93mp4.execute-api.us-west-2.amazonaws.com/dev/authentication/set-password", this.state)
            .then(result => {
                NotificationManager.success(result.data.message);
                this.props.history.push({pathname:'login'});
            })
            .catch(err => {
                if (err.response && err.response.status === 404)
                    NotificationManager.error(err.response.data.message);
                else
                    NotificationManager.error("Something Went Wrong");
                this.setState({ errors: err.response });
            });

    }
    render() {
        const { isEmail } = this.state;
        return (
            <div className="content">
                <NotificationContainer />
                <form onSubmit={this.handleForm}>
                    <div className="row" style={{ marginTop: 20 }}>
                        <div className="col-sm-3"></div>
                        <div className="col-sm-6">
                            <div className="card">
                                <div className="card-header text-center">Reset Password</div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label >Medium</label>
                                        <input type="text" name="medium" value={this.state.medium} disabled={isEmail} onChange={this.handleInput} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label >Email or Phone</label>
                                        <input type="text" name="emailOrPhone" value={this.state.emailOrPhone} disabled={isEmail} onChange={this.handleInput} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label >New Password</label>
                                        <input type="password" name="newPassword" value={this.state.newPassword} onChange={this.handleInput} className="form-control" />
                                    </div>
                                    {!isEmail ? <div className="form-group">
                                            <label >Confirmation Code</label>
                                            <input type="text" name="confirmationCode" value={this.state.confirmationCode} onChange={this.handleInput} className="form-control" />
                                        </div> : null
                                    }
                                </div>
                                <div className="card-footer text-center">
                                    <input type="button" value="Reset" onClick={this.handleForm} className="btn btn-primary" />
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-3"></div>
                    </div>
                </form>
            </div>
        )
    }
}